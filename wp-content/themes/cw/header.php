<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7]> <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html style="margin-top: 0!important;" class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html style="margin-top: 0!important;" class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<!-- <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png"/> -->

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	<!-- WP_HEAD() -->
	<?php wp_head(); ?>

	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
	<![endif]-->
	<script>
		$( document ).ready(function() {

		});
	</script>

</head>

<body <?php body_class(); ?>>
	<a id="top"></a>
	<header role="banner ">
		<div class="row banner_bg">
			<div class="small-12 medium-6 large-6 columns">
				<?php $logo_url = cw_options_get_option( 'cwo_logo' ); ?>
				<h1 class="logo center">
					<a href="/" title="<?php bloginfo( 'name' ); ?>">
						<?php if(!empty($logo_url)) { ?>
							<img class="site_logo" src="<?php echo $logo_url; ?>" alt="<?php bloginfo( 'name' ); ?>" />
						<?php } else {
							bloginfo( 'name' );
						} ?>
					</a>
				</h1>
			</div>

			<div class="center phone_number"><h5><a href="tel:18884482577" class="w_txt">(888) 448-2577</a></h5></div>
			<div class="small-12 medium-6 large-6 columns menu_pad">
				<nav class="top-bar" data-topbar role="navigation">
					<ul class="title-area">
						<li class="name"></li>
						<li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
					</ul>

					<section class="top-bar-section">
						<ul class="menu">
							<li class="menu-item"><a href="#sell">WHY SELL</a></li>
							<li class="menu-item"><a href="#buy">WHY WE BUY</a></li>
							<li class="menu-item"><a href="#offers">OFFERS</a></li>
							<li class="menu-item"><a href="#contact">CONTACT</a></li>
							<!-- <li class="menu-item"><a href="#bottom" id="top">Click to scroll down</a></li> -->
						</ul>
						
					</section>
				</nav> 
			</div>

		</div>	

		<div class="nav-container contain-to-grid">
			<!-- <nav class="top-bar" data-topbar role="navigation">
				<ul class="title-area">
					<li class="name"></li>
					<li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
				</ul>

				<section class="top-bar-section">
					<?php 
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'container' => '',
								'menu_class' => 'menu',
								'depth' => 2,
								'fallback_cb' => 'wp_page_menu',
								'walker' => new Foundation_Walker_Nav_Menu()
							)
						);
					?>
				</section>
			</nav> -->
		</div>
	</header>