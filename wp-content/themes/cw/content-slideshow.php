<?php
	global $post;
	$page_id = $post->ID;
	
	$slideshow_args = array(
		'post_type' => 'slideshows',
		'posts_per_page' => -1,
		'meta_key' => '_cwmb_slideshow_page',
		'meta_value' => $page_id
	);

	$slideshow = new WP_Query($slideshow_args);

	if($slideshow->have_posts()){
		while($slideshow->have_posts()) {
			$slideshow->the_post();
			echo '<div class="cw-slideshow">';
			$slides = get_post_meta($post->ID, '_cwmb_slideshow', true);

			if(!empty($slides)) {
				echo '<ul class="styleless cw-slider">';
					foreach ($slides as $slide) {
						$cropped = aq_resize( $slide['image'], 1000, 500, true, true, true );

						echo '<li class="slide">';
							if(!empty($slide['link'])) { echo '<a href="'.$slide['link'].'">'; }
								
								if(!empty($slide['image'])) { echo '<img src="'.$cropped.'" alt="" />'; }
								$class = '';
								if(!empty($slide['title']) || !empty($slide['caption'])) {
									$class = 'has-cap';
								}
								// echo '<div class="slide-words '.$class.'">';
								// 	if(!empty($slide['title'])) { echo '<h3 class="slide-title">'.$slide['title'].'</h3>'; }
								// 	if(!empty($slide['caption'])) { echo '<p class="slide-caption">'.$slide['caption'].'</p>'; }
								// echo '</div>';

							if(!empty($slide['link'])) { echo '</a>'; }
						echo '</li>';
					}
				echo '</ul>';
			}
			echo '</div>';
		}
	}
wp_reset_query(); ?>