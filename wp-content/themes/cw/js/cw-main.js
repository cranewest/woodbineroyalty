jQuery(document).ready(function($){
	// uncomment below to support placeholders in < IE10
	// $('input, textarea').placeholder();
	// for debugging
	$('pre').each(function(){
		$(this).find('.close-pre').click(function(){
			$(this).toggleClass('open');
			$(this).next('.content').slideToggle();
		});
	});
	
	if( !$("html").hasClass("lt-ie9") ) {
		$(document).foundation();
		console.log("Foundation Js loaded");
	}

	// do this for each slider, keeps them from transitioning with only one slide
	var slides_count = $('.cw-slider li').length;
	if(slides_count > 1) {
		$('.cw-slider').bxSlider({
			mode: 'fade',
			controls: false,
			auto: true,
			autoStart: true
		});
	}

	// cw_accordion
	// $('.cwa-section-header').each(function() {
	// 	if($(this).hasClass('open-header')) {
	// 		$(this).next('.cwa-section-content').slideDown(400).addClass('open-tab');
	// 	}
	// });

	// $('.cwa-section-header').click(function(){
		
	// 	if(!$(this).hasClass('open-header')) {
	// 		$('.open-header').removeClass('open-header');
	// 		$(this).addClass('open-header');

	// 	} else if($(this).hasClass('open-header')) {
	// 		$(this).removeClass('open-header');
	// 	}

	// 	$('.cwa-section-content').slideUp(400).removeClass('open-tab');

	// 	if($(this).next('.cwa-section-content').is(':visible')){
	// 		$(this).next('.cwa-section-content').slideUp(400).removeClass('open-tab');
	// 	} else {
	// 		$(this).next('.cwa-section-content').slideDown(400).addClass('open-tab');
	// 	}
	// });

	// hide captchas (gravity forms)
	$('.gform_wrapper').click(function(event){
		$(this).find('.gf_captcha').slideDown();
		event.stopPropagation();
	});

	$('html').click(function() {
		$('.gf_captcha').slideUp();
	});

	//scroll animation 
	$('a[href^="#"]').on('click', function(event) {

	    var target = $( $(this).attr('href') );

	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').animate({
	            scrollTop: target.offset().top
	        }, 1000);
	    }

	});
});