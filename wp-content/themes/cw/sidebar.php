<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
	<aside class="widget-area small-12 medium-4 large-4 columns" role="complementary">
		<?php get_search_form( true ); ?>
	</aside>