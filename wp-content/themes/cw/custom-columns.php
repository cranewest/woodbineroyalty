<?php
// add custom columns in admin

function add_slideshow_columns($columns) {
    return array_merge($columns, 
		array(
			'thumbnail' => __('Thumbnail')
		)
	);
}
add_filter('manage_slideshow_posts_columns' , 'add_slideshow_columns');

function custom_slideshow_column( $column, $post_id ) {
	global $post;
	$post_id = $post->ID;
	
	switch ( $column ) {
		case 'thumbnail':
		echo '<img src="'.get_post_meta( $post_id , 'admin_thumb' , true ).'" alt="" />';
		break;
	}
}
add_action( 'manage_slideshow_posts_custom_column' , 'custom_slideshow_column' );

// add team to players list in admin
function add_players_column($columns) {
    return array_merge($columns, 
		array(
			'team' => __('Team'),
		)
	);
}
add_filter('manage_players_posts_columns' , 'add_players_column');

function custom_players_column( $column, $post_id ) {
	global $post;
	$post_id = $post->ID;
	
	$team_ids = get_post_meta($post->ID, '_cmb_player_team', true);

	$output = '';

	if(!empty($team_ids)) {
		foreach($team_ids as $key => $team_id) {
			$team = get_term_by('term_taxonomy_id', $team_id, 'sport_teams');
			if(!empty($team) && $key == 0) {
				$output .= $team->name;
			} elseif (!empty($team) && $key > 0) {
				$output .= ', '.$team->name;
			}
		}
	} else {
		$output = 'None';
	}

	if($output == '') {
		$output = 'None';
	}

	switch ( $column ) {
		case 'team':
		echo $output;
		break;
	}
}
add_action( 'manage_players_posts_custom_column' , 'custom_players_column' );

function sortable_by_team( $columns ) {
    $columns['team'] = 'team';
 
    //To make a column 'un-sortable' remove it from the array
    //unset($columns['date']);
 
    return $columns;
}
add_filter( 'manage_edit-players_sortable_columns', 'sortable_by_team' );