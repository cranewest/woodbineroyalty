<?php
// ------------------------------------
//
// Custom Meta Boxes
//
// ------------------------------------
// custom meta boxes
function list_pages() {
	$pages_args = array(
		'sort_order' => 'ASC',
		'sort_column' => 'post_title'
		);

	$pages = get_pages($pages_args);

	global $cw_page_list;
	$cw_page_list = array('' => 'Select a page');
	foreach($pages as $page) {
		$page_name = $page->post_title;
		$page_id = $page->ID;
		$cw_page_list[$page_id] = $page_name;
	}
}
list_pages();

function cw_metaboxes( array $meta_boxes ) {
	global $cw_page_list;
	// use for select, checkbox, radio of list of states
	global $cw_states;

	$prefix = '_cwmb_'; // Prefix for all fields

 	$meta_boxes['slideshows'] = array(
 		'id'         => 'slideshows',
 		'title'      => 'Slideshow Info',
 		'object_types'      => array( 'slideshows' ), // Post type
 		'context'    => 'normal',
 		'priority'   => 'high',
 		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
			    'id'          => $prefix . 'slideshow',
			    'type'        => 'group',
			    'description' => '',
			    'options'     => array(
			        'group_title'   => 'Slide {#}',
			        'add_button'    => 'Add another slide',
			        'remove_button' => 'Remove slide',
			        'sortable'      => true, // beta
			    ),
			    'fields'      => array(
			        array(
			            'name' => 'Title',
			            'id'   => 'title',
			            'type' => 'text',
			        ),
			        array(
			        	'name' => 'Link',
			        	'id' => 'link',
			        	'type' => 'text_url'
			        ),
			        array(
			        	'name' => 'Image',
			        	'id' => 'image',
			        	'type' => 'file'
			        ),
			        array(
			        	'name' => 'Caption',
			        	'id' => 'caption',
			        	'type' => 'textarea'
			        )
			    )
			)
 		)
 	);

 	$meta_boxes['slideshow_page'] = array(
 		'id'         => 'slideshow_page',
 		'title'      => 'Select Page(s)',
 		'object_types'      => array( 'slideshows' ), // Post type
 		'context'    => 'side',
 		'priority'   => 'high',
 		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => '',
				'desc' => 'Choose page(s) to show this slideshow on. NOTE: If no page is selected, this slideshow will note be shown anywhere on the site.',
				'id' => $prefix.'slideshow_page',
				'type' => 'select',
				'options' => $cw_page_list
			)
 		)
 	);

	// promos
	$meta_boxes['promos'] = array(
		'id' => 'promos',
		'title' => 'Promo Info',
		'object_types' => array('promos'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Promo Image',
				'id' => $prefix.'promo_image',
				'type' => 'file'
			),
			array(
				'name' => 'Link',
				'id' => $prefix.'promo_link',
				'type' => 'text_url'
			)
		),
	);

	// services
	$meta_boxes['services'] = array(
		'id' => 'services',
		'title' => 'Options',
		'object_types' => array('services'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'side',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array (
				'name' => 'Service Image',
				'id' => $prefix.'service_image',
				'type' => 'file'
			)
		),
	);

	$meta_boxes['testimonials'] = array(
		'id' => 'testimonials',
		'title' => 'Testimonial',
		'object_types' => array('testimonials'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'id' => $prefix.'testimonial',
				'type' => 'textarea'
			),
			array(
				'name' => 'Vocation',
				'id' => $prefix.'vocation',
				'type' => 'text'
			),
			array(
				'name' => 'Location',
				'id' => $prefix.'location',
				'type' => 'text'
			),			
		),
	);

	$meta_boxes['staff'] = array(
		'id' => 'staff',
		'title' => 'Team Member Info',
		'object_types' => array('staff'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Title',
				'id' => $prefix.'staff_title',
				'type' => 'text'
			),
			array(
				'name' => 'Image',
				'id' => $prefix.'staff_image',
				'type' => 'file'
			),
			array(
				'name' => 'Bio',
				'id' => $prefix.'staff_bio',
				'type' => 'textarea'
			),			
		),
	);

	//home page meta boxes
	$prefix='_cwmb_';
	$meta_boxes['sell'] = array(
		'id' => 'sell',
		'title' => 'Why Sell Royalties',
		'object_types' => array('page'), // post type
		'show_on' => array( 'key' => 'page-template', 'value' => 'page-home.php' ), // Limit to page template
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, 
		'fields' => array(
			array(
			    'id'          => $prefix . 'sell_list',
			    'type'        => 'group',
			    'description' => '',
			    'options'     => array(
			        'group_title'   => 'Why Sell Royalties {#}', // since version 1.1.4, {#} gets replaced by row number
			        'add_button'    => 'Add Another',
			        'remove_button' => 'Remove',
			        'sortable'      => true, // beta
			    ),

				'fields'       => array(
					array(
						'name' => 'Title',
						'id' => $prefix.'sell_title',
						'type' => 'text'
					),
			        array(
					    'name' => 'Sell Description',
					    'id' => $prefix . 'sell_desc',
					    'type' => 'textarea_small'
					),

			    ),
			),
		),
	);

	$meta_boxes['buy'] = array(
		'id' => 'buy',
		'title' => 'Why We Buy Interest',
		'object_types' => array('page'), // post type
		'show_on' => array( 'key' => 'page-template', 'value' => 'page-home.php' ), // Limit to page template
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, 
		'fields' => array(
			array(
		            'name' => 'Buy Interest Content',
		            'id'   => $prefix.'buy_desc',
		            'type' => 'wysiwyg',
		        )
		),
	);

	$meta_boxes['offer'] = array(
		'id' => 'offer',
		'title' => 'Offers',
		'object_types' => array('page'), // post type
		'show_on' => array( 'key' => 'page-template', 'value' => 'page-home.php' ), 
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, 
		'fields' => array(
			array(
		            'name' => 'If you received an Offer',
		            'id'   => $prefix.'offer_desc',
		            'type' => 'wysiwyg',
		        ),
			array(
		            'name' => 'If you have not received an Offer',
		            'id'   => $prefix.'no_offer_desc',
		            'type' => 'wysiwyg',
		        )
		),
	);

	$meta_boxes['g_form'] = array(
		'id' => 'g_form',
		'title' => 'Gravity Forms Shortcode',
		'object_types' => array('page'), // post type
		'show_on' => array( 'key' => 'page-template', 'value' => 'page-home.php' ), // Limit to page template
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, 
		'fields' => array(
			array(
		            'name' => 'Shortcode',
		            'id'   => $prefix.'s_code',
		            'type' => 'text',
		        )
		),
	);


	//example of repeating metaboxes for page template
	// $meta_boxes['careers'] = array(
	// 	'id' => 'careers',
	// 	'title' => 'Career Opportunities',
	// 	'object_types' => array('page'), // post type
	// 	// 'show_on' => array( 'key' => 'page-template', 'value' => 'page-templates/page-template-file-here.php' ), // Limit to page template
	// 	'context' => 'normal',
	// 	'priority' => 'default',
	// 	'show_names' => true, // Show field names on the left
	// 	'fields' => array(
	// 		array(
	// 		    'id'          => $prefix . 'careers',
	// 		    'type'        => 'group',
	// 		    'description' => '',
	// 		    'options'     => array(
	// 		        'group_title'   => 'Tab {#}', // since version 1.1.4, {#} gets replaced by row number
	// 		        'add_button'    => 'Add another item',
	// 		        'remove_button' => 'Remove item',
	// 		        'sortable'      => true, // beta
	// 		    ),
	// 		    // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
	// 		    'fields'      => array(
	// 		        array(
	// 		            'name' => 'Career Title',
	// 		            'id'   => 'career_title',
	// 		            'type' => 'text',
	// 		        ),
	// 		        array(
	// 		            'name' => 'Career Content',
	// 		            'id'   => 'career_content',
	// 		            'type' => 'wysiwyg',
	// 		        )
	// 		    ),
	// 		)
	// 	),
	// );

	return $meta_boxes;
}
add_filter( 'cmb2_meta_boxes', 'cw_metaboxes' );

// end custom meta boxes