<?php
/**
 * Staff Custom Post Type
 *
 * @since CW 1.0
 */
function cw_cpp_locations_init() {
	// single uppercase
	$su = 'Location';
	// plural uppercase
	$pu = 'Locations';
	// single lowercase
	$sl = 'location';
	// plural lowercase
	$pl = 'locations';

	$field_args = array(
		'labels' => array(
			'name' => __( $pu ),
			'singular_name' => __( $pu ),
			'add_new' => __( 'Add New '.$su ),
			'add_new_item' => __( 'Add New '.$su ),
			'edit_item' => __( 'Edit '.$su ),
			'new_item' => __( 'Add New '.$su ),
			'view_item' => __( 'View '.$su ),
			'search_items' => __( 'Search '.$pu ),
			'not_found' => __( 'No '.$pl.' found' ),
			'not_found_in_trash' => __( 'No '.$pl.' found in trash' )
		),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => true,
		'has_archive' => true,
		'menu_position' => 18,
		'supports' => array('title', 'thumbnail')
	);
	register_post_type('locations',$field_args);
}
add_action( 'init', 'cw_cpp_locations_init' );


/**
 * Add Categories to Custom Post Type
 *
 * This is just an example of adding categories to a custom post type.
 * To see in action just uncomment and it will add categories to locations.
 *
 * @since CW 1.0
 */
// function cw_cpp_locations_categories() {
// 	$field_args = array(
// 		'labels' => array(
// 			'name'              => _x( 'Categories', 'taxonomy general name' ),
// 			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
// 			'search_items'      => __( 'Search Categories' ),
// 			'all_items'         => __( 'All Categories' ),
// 			'parent_item'       => __( 'Parent Category' ),
// 			'parent_item_colon' => __( 'Parent Category:' ),
// 			'edit_item'         => __( 'Edit Category' ),
// 			'update_item'       => __( 'Update Category' ),
// 			'add_new_item'      => __( 'Add New Category' ),
// 			'new_item_name'     => __( 'New Category' ),
// 			'menu_name'         => __( 'Categories' ),
// 		),
// 		'hierarchical' => true
// 	);
// 	register_taxonomy( 'locations_categories', 'locations', $field_args );
// }
// add_action( 'init', 'cw_cpp_locations_categories', 0 );



/**
 * Listings Custom Fields
 *
 * @since CW 1.0
 */
function cw_cpp_location_metaboxes() {
	add_meta_box('cw_cpp_location_meta', 'Phone', 'cw_cpp_location_meta', 'locations', 'normal', 'default');
}
add_action( 'add_meta_boxes', 'cw_cpp_location_metaboxes' );

function cw_cpp_location_meta() {
	global $post;

	// Noncename needed to verify where the data originated
 	echo '<input type="hidden" name="location_meta_noncename" id="location_meta_noncename" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// Get the data if there is any.
	$address_1 = get_post_meta($post->ID, '_address_1', true);

	// Echo out the field
	echo '<h2 style="margin-bottom: 0;">Address 1</h2>';
	echo '<input type="text" name="_address_1" class="widefat" value="' . $address_1  . '" />';

	// Get the data if there is any.
	$address_2 = get_post_meta($post->ID, '_address_2', true);

	// Echo out the field
	echo '<h2 style="margin-bottom: 0;">Address 2</h2>';
	echo '<input type="text" name="_address_2" class="widefat" value="' . $address_2  . '" />';

	// Get the data if there is any.
	$city = get_post_meta($post->ID, '_city', true);

	// Echo out the field
	echo '<h2 style="margin-bottom: 0;">City</h2>';
	echo '<input type="text" name="_city" class="widefat" value="' . $city  . '" />';

	// Get the data if there is any.
	$state = get_post_meta($post->ID, '_state', true);

	// Echo out the field
	echo '<h2 style="margin-bottom: 0;">State</h2>';
	echo '<input type="text" name="_state" class="widefat" value="' . $state  . '" />';

	// Get the data if there is any.
	$zip = get_post_meta($post->ID, '_zip', true);

	// Echo out the field
	echo '<h2 style="margin-bottom: 0;">Zip</h2>';
	echo '<input type="text" name="_zip" class="widefat" value="' . $zip  . '" />';

	// Get the data if there is any.
	$phone = get_post_meta($post->ID, '_phone', true);

	// Echo out the field
	echo '<h2 style="margin-bottom: 0;">Phone Number</h2>';
	echo '<input type="text" name="_phone" class="widefat" value="' . $phone  . '" />';

	// Get the data if there is any.
	$toll_free = get_post_meta($post->ID, '_toll_free', true);

	// Echo out the field
	echo '<h2 style="margin-bottom: 0;">Toll Free Phone Number</h2>';
	echo '<input type="text" name="_toll_free" class="widefat" value="' . $toll_free  . '" />';

	// Get the data if there is any.
	$fax = get_post_meta($post->ID, '_fax', true);

	// Echo out the field
	echo '<h2 style="margin-bottom: 0;">Fax</h2>';
	echo '<input type="text" name="_fax" class="widefat" value="' . $fax  . '" />';

	// Get the data if there is any.
	$email = get_post_meta($post->ID, '_email', true);

	// Echo out the field
	echo '<h2 style="margin-bottom: 0;">Email</h2>';
	echo '<input type="text" name="_email" class="widefat" value="' . $email  . '" />';

	// Get the data if there is any.
	$link = get_post_meta($post->ID, '_link', true);

	// Echo out the field
	echo '<h2 style="margin-bottom: 0;">Link (including http://)</h2>';
	echo '<input type="text" name="_link" class="widefat" placeholder="http://" value="' . $link  . '" />';
}

function cw_cpp_location_save_meta($post_id, $post) {

	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( !wp_verify_nonce( $_POST['location_meta_noncename'], plugin_basename(__FILE__) )) {
	return $post->ID;
	}

	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	// OK, we're authenticated: we need to find and save the data
	// We'll put it into an array to make it easier to loop though.
	$meta['_address_1'] = $_POST['_address_1'];
	$meta['_address_2'] = $_POST['_address_2'];
	$meta['_city'] = $_POST['_city'];
	$meta['_state'] = $_POST['_state'];
	$meta['_zip'] = $_POST['_zip'];
	$meta['_phone'] = $_POST['_phone'];
	$meta['_toll_free'] = $_POST['_toll_free'];
	$meta['_fax'] = $_POST['_fax'];
	$meta['_email'] = $_POST['_email'];
	$meta['_link'] = $_POST['_link'];

	// Add values of $meta as custom fields
	foreach ($meta as $key => $value) { // Cycle through the $meta array!
		if( $post->post_type == 'revision' ) return; // Don't store custom data twice
		$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
		if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, $key, $value);
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, $key, $value);
		}
		if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
	}
}
add_action('save_post', 'cw_cpp_location_save_meta', 1, 2); // save the custom fields