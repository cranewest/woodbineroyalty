<?php
// add_action( 'wp_login', 'redirect_non_admins', 10, 2 );
function redirect_non_admins( $user_id, $user ) {
	$admin_roles = array('administrator', 'emsadmin');

	if( 0 == count( array_intersect( $admin_roles, $user->roles ) ) ) {
		wp_redirect( home_url() );
		exit;
	}
}

function cw_metaboxes( $meta_boxes ) {
	// Used for generating a select of states.
    // $states = array(
    //     ''  => '',
    // 	'ALABAMA' => 'AL',
    // 	'ALASKA' => 'AK',
    // 	'AMERICAN SAMOA' => 'AS',
    // 	'ARIZONA' => 'AZ',
    // 	'ARKANSAS' => 'AR',
    // 	'CALIFORNIA' => 'CA',
    // 	'COLORADO' => 'CO',
    // 	'CONNECTICUT' => 'CT',
    // 	'DELAWARE' => 'DE',
    // 	'DISTRICT OF COLUMBIA' => 'DC',
    // 	'FEDERATED STATES OF MICRONESIA' => 'FM',
    // 	'FLORIDA' => 'FL',
    // 	'GEORGIA' => 'GA',
    // 	'GUAM GU' => 'GU',
    // 	'HAWAII' => 'HI',
    // 	'IDAHO' => 'ID',
    // 	'ILLINOIS' => 'IL',
    // 	'INDIANA' => 'IN',
    // 	'IOWA' => 'IA',
    // 	'KANSAS' => 'KS',
    // 	'KENTUCKY' => 'KY',
    // 	'LOUISIANA' => 'LA',
    // 	'MAINE' => 'ME',
    // 	'MARSHALL ISLANDS' => 'MH',
    // 	'MARYLAND' => 'MD',
    // 	'MASSACHUSETTS' => 'MA',
    // 	'MICHIGAN' => 'MI',
    // 	'MINNESOTA' => 'MN',
    // 	'MISSISSIPPI' => 'MS',
    // 	'MISSOURI' => 'MO',
    // 	'MONTANA' => 'MT',
    // 	'NEBRASKA' => 'NE',
    // 	'NEVADA' => 'NV',
    // 	'NEW HAMPSHIRE' => 'NH',
    // 	'NEW JERSEY' => 'NJ',
    // 	'NEW MEXICO' => 'NM',
    // 	'NEW YORK' => 'NY',
    // 	'NORTH CAROLINA' => 'NC',
    // 	'NORTH DAKOTA' => 'ND',
    // 	'NORTHERN MARIANA ISLANDS' => 'MP',
    // 	'OHIO' => 'OH',
    // 	'OKLAHOMA' => 'OK',
    // 	'OREGON' => 'OR',
    // 	'PALAU' => 'PW',
    // 	'PENNSYLVANIA' => 'PA',
    // 	'PUERTO RICO' => 'PR',
    // 	'RHODE ISLAND' => 'RI',
    // 	'SOUTH CAROLINA' => 'SC',
    // 	'SOUTH DAKOTA' => 'SD',
    // 	'TENNESSEE' => 'TN',
    // 	'TEXAS' => 'TX',
    // 	'UTAH' => 'UT',
    // 	'VERMONT' => 'VT',
    // 	'VIRGIN ISLANDS' => 'VI',
    // 	'VIRGINIA' => 'VA',
    // 	'WASHINGTON' => 'WA',
    // 	'WEST VIRGINIA' => 'WV',
    // 	'WISCONSIN' => 'WI',
    // 	'WYOMING' => 'WY',
    // 	'ARMED FORCES AFRICA \ CANADA \ EUROPE \ MIDDLE EAST' => 'AE',
    // 	'ARMED FORCES AMERICA (EXCEPT CANADA)' => 'AA',
    // 	'ARMED FORCES PACIFIC' => 'AP',
    // );

    // $prefix = '_directory_'; // Prefix for all fields
    // $meta_boxes[] = array(
    //     'id' => 'directory_meta',
    //     'title' => 'Listing Information',
    //     'pages' => array('directory'), // post type
    //     'context' => 'normal',
    //     'priority' => 'high',
    //     'show_names' => true, // Show field names on the left
    //     'fields' => array(
    //         array(
    //             'name' => 'Address',
    //             'desc' => '',
    //             'id'   => $prefix . 'directory',
    //             'type' => 'text'
    //         ),
    //         array(
    //             'name' => 'City',
    //             'desc' => '',
    //             'id'   => $prefix . 'city',
    //             'type' => 'text'
    //         ),
    //         array(
    //             'name'    => 'State',
    //             'desc'    => '',
    //             'id'      => $prefix . 'state',
    //             'type'    => 'select',
    //             'options' => $states
    //         ),
    //         array(
    //             'name' => 'Zipcode',
    //             'desc' => '',
    //             'id'   => $prefix . 'zip',
    //             'type' => 'text'
    //         ),
    //         array(
    //             'name' => 'Office Phone Number',
    //             'desc' => '',
    //             'id'   => $prefix . 'office_phone',
    //             'type' => 'text'
    //         ),
    //         array(
    //             'name' => 'Contact Phone Number',
    //             'desc' => '',
    //             'id'   => $prefix . 'contact_phone',
    //             'type' => 'text'
    //         ),
    //         array(
    //             'name' => 'Fax',
    //             'desc' => '',
    //             'id'   => $prefix . 'fax',
    //             'type' => 'text'
    //         ),
    //     ),
    // );

    // $prefix = '_downloads_';

    // $meta_boxes[] = array(
    //     'id' => 'download_meta',
    //     'title' => 'Download Information',
    //     'pages' => array('downloads'), // post type
    //     'context' => 'normal',
    //     'priority' => 'high',
    //     'show_names' => true, // Show field names on the left
    //     'fields' => array(
    //         array(
    //             'name' => 'Download',
    //             'desc' => 'Upload an image or enter an URL.',
    //             'id' => $prefix . 'file',
    //             'type' => 'file',
    //             'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
    //         ),
    //     ),
    // );

 //    $prefix = '_event_';
	// $meta_boxes[] = array(
	//     'id' => 'events_meta',
	//     'title' => 'Event Information',
	//     'pages' => array('events'), // post type
	//     'context' => 'normal',
	//     'priority' => 'high',
	//     'show_names' => true, // Show field names on the left
	//     'fields' => array(
	//     	array(
	//     	    'name' => 'Start Date',
	//     	    'desc' => 'Event start date and time.',
	//     	    'id' => $prefix . 'event_start',
	//     	    'type' => 'text_datetime_timestamp'
	//     	),
	//     	array(
	//     	    'name' => 'End Date',
	//     	    'desc' => 'Event end date and time',
	//     	    'id' => $prefix . 'event_end',
	//     	    'type' => 'text_datetime_timestamp'
	//     	),
	//     	array(
	//     	    'name' => 'Address 1',
	//     	    'desc' => '',
	//     	    'std' => '',
	//     	    'id' => $prefix . 'address1',
	//     	    'type' => 'text'
	//     	),
	//     	array(
	//     	    'name' => 'Address 2',
	//     	    'desc' => '',
	//     	    'std' => '',
	//     	    'id' => $prefix . 'address2',
	//     	    'type' => 'text'
	//     	),
	//     	array(
	//     	    'name' => 'City',
	//     	    'desc' => '',
	//     	    'std' => '',
	//     	    'id' => $prefix . 'city',
	//     	    'type' => 'text'
	//     	),
	//     	array(
 //                  'name'    => 'State',
 //                  'desc'    => '',
 //                  'id'      => $prefix . 'state',
 //                  'type'    => 'select',
 //                  'options' => $states
 //              ),
	//     	array(
	//     	    'name' => 'Zip Code',
	//     	    'desc' => '',
	//     	    'std' => '',
	//     	    'id' => $prefix . 'zip',
	//     	    'type' => 'text'
	//     	),
	//     )
	// );

 //    $prefix = '_slideshow_';
 //    $meta_boxes[] = array(
 //        'id' => 'slideshow_meta',
 //        'title' => 'Event Information',
 //        'pages' => array('slideshow'), // post type
 //        'context' => 'normal',
 //        'priority' => 'high',
 //        'show_names' => true, // Show field names on the left
 //        'fields' => array(
 //           array(
 //                'name' => 'Slide Link',
 //                'id'   => $prefix . 'link',
 //                'type' => 'text_url',
 //                'protocols' => array( 'http', 'https' ), // Array of allowed protocols
 //            ),
 //            array(
 //                'name' => 'Caption',
 //                'desc' => '',
 //                'default' => '',
 //                'id' => $prefix . 'caption',
 //                'type' => 'textarea_small'
 //            ),
 //        )
 //    );

    return $meta_boxes;
}
// add_filter( 'cmb_meta_boxes', 'cw_metaboxes' );

function init_cw_metaboxes() {
    if ( !class_exists( 'cmb_Meta_Box' ) ) {
        require_once( 'lib/php/metabox/init.php' );
    }
}
// add_action( 'init', 'init_cw_metaboxes', 9999 );