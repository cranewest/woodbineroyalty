<?php
/**
 * Links Custom Post Type
 *
 * @since CW 1.0
 */
function cw_cpp_links_init() {
	$field_args = array(
		'labels' => array(
			'name' => __( 'Links' ),
			'singular_name' => __( 'Links' ),
			'add_new' => __( 'Add New Link' ),
			'add_new_item' => __( 'Add New Link' ),
			'edit_item' => __( 'Edit Link' ),
			'new_item' => __( 'Add New Link' ),
			'view_item' => __( 'View Link' ),
			'search_items' => __( 'Search Links' ),
			'not_found' => __( 'No links found' ),
			'not_found_in_trash' => __( 'No links found in trash' )
		),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => true,
		'has_archive' => true,
		'menu_position' => 20,
		'supports' => array('title')
	);
	register_post_type('links',$field_args);
}
add_action( 'init', 'cw_cpp_links_init' );



/**
 * Tweaks to Featured Image on Links
 *
 * Moves Featured Image field from sidebar to main on links and renames it.
 *
 * @since CW 1.0
 */
// function cw_cpp_links_move_image_box() {
// 	remove_meta_box( 'postimagediv', 'links', 'side' );
// 	add_meta_box('postimagediv', __('Link Image'), 'post_thumbnail_meta_box', 'links', 'normal', 'high');
// }
// add_action('do_meta_boxes', 'cw_cpp_links_move_image_box');



/**
 * Add Categories to Custom Post Type
 *
 * This is just an example of adding categories to a custom post type.
 * To see in action just uncomment and it will add categories to links.
 *
 * @since CW 1.0
 */
// function cw_cpp_links_categories() {
// 	$field_args = array(
// 		'labels' => array(
// 			'name'              => _x( 'Categories', 'taxonomy general name' ),
// 			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
// 			'search_items'      => __( 'Search Categories' ),
// 			'all_items'         => __( 'All Categories' ),
// 			'parent_item'       => __( 'Parent Category' ),
// 			'parent_item_colon' => __( 'Parent Category:' ),
// 			'edit_item'         => __( 'Edit Category' ),
// 			'update_item'       => __( 'Update Category' ),
// 			'add_new_item'      => __( 'Add New Category' ),
// 			'new_item_name'     => __( 'New Category' ),
// 			'menu_name'         => __( 'Categories' ),
// 		),
// 		'hierarchical' => true
// 	);
// 	register_taxonomy( 'links_categories', 'links', $field_args );
// }
// add_action( 'init', 'cw_cpp_links_categories', 0 );



/**
 * Links Custom Fields
 *
 * @since CW 1.0
 */
function cw_cpp_links_metaboxes() {
	add_meta_box('links_meta', 'Link URL (Including "http://")', 'cw_cpp_links_meta', 'links', 'normal', 'default');
}
add_action( 'add_meta_boxes', 'cw_cpp_links_metaboxes' );

function cw_cpp_links_meta() {
	global $post;
	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="links_meta_noncename" id="links_meta_noncename" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// Get the data if there is any.
	$url = get_post_meta($post->ID, '_links_url', true);

	// Echo out the field
	echo '<input type="text" name="_links_url" class="widefat" placeholder="http://" value="' . $url  . '"/>';
}
function cw_cpp_links_save_meta($post_id, $post) {

	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( !wp_verify_nonce( $_POST['links_meta_noncename'], plugin_basename(__FILE__) )) {
	return $post->ID;
	}

	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	// OK, we're authenticated: we need to find and save the data
	// We'll put it into an array to make it easier to loop though.
	$meta['_links_url'] = $_POST['_links_url'];

	// Add values of $meta as custom fields
	foreach ($meta as $key => $value) { // Cycle through the $meta array!
		if( $post->post_type == 'revision' ) return; // Don't store custom data twice
		$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
		if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, $key, $value);
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, $key, $value);
		}
		if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
	}
}
add_action('save_post', 'cw_cpp_links_save_meta', 1, 2); // save the custom fields