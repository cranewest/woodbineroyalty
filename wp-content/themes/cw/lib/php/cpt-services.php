<?php
/**
 * Services Custom Post Type
 *
 * @since CW 1.0
 */
function cw_cpp_services_init() {
	$field_args = array(
		'labels' => array(
			'name' => __( 'Services' ),
			'singular_name' => __( 'Services' ),
			'add_new' => __( 'Add New Service' ),
			'add_new_item' => __( 'Add New Service' ),
			'edit_item' => __( 'Edit Service' ),
			'new_item' => __( 'Add New Service' ),
			'view_item' => __( 'View Service' ),
			'search_items' => __( 'Search Services' ),
			'not_found' => __( 'No services found' ),
			'not_found_in_trash' => __( 'No services found in trash' )
		),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => true,
		'rewrite' => true,
		'menu_position' => null,
		'supports' => array('title', 'editor')
	);
	register_post_type('services',$field_args);
}
add_action( 'init', 'cw_cpp_services_init' );



/**
 * Tweaks to Featured Image on Services
 *
 * Moves Featured Image field from sidebar to main on services and renames it.
 *
 * @since CW 1.0
 */
// function cw_cpp_services_move_image_box() {
// 	remove_meta_box( 'postimagediv', 'services', 'side' );
// 	add_meta_box('postimagediv', __('Service Image'), 'post_thumbnail_meta_box', 'services', 'normal', 'high');
// }
// add_action('do_meta_boxes', 'cw_cpp_services_move_image_box');



/**
 * Add Categories to Custom Post Type
 *
 * This is just an example of adding categories to a custom post type.
 * To see in action just uncomment and it will add categories to services.
 *
 * @since CW 1.0
 */
// function cw_cpp_services_categories() {
// 	$field_args = array(
// 		'labels' => array(
// 			'name'              => _x( 'Categories', 'taxonomy general name' ),
// 			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
// 			'search_items'      => __( 'Search Categories' ),
// 			'all_items'         => __( 'All Categories' ),
// 			'parent_item'       => __( 'Parent Category' ),
// 			'parent_item_colon' => __( 'Parent Category:' ),
// 			'edit_item'         => __( 'Edit Category' ),
// 			'update_item'       => __( 'Update Category' ),
// 			'add_new_item'      => __( 'Add New Category' ),
// 			'new_item_name'     => __( 'New Category' ),
// 			'menu_name'         => __( 'Categories' ),
// 		),
// 		'hierarchical' => true
// 	);
// 	register_taxonomy( 'services_categories', 'services', $field_args );
// }
// add_action( 'init', 'cw_cpp_services_categories', 0 );