<?php
/**
 * Template Name: Home Page 
 * Description: Custom home page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>

<? get_header(); ?>
<!-- <div class="bg_shadow"> -->
	<div class="row">
		<div class="slideshow_shadow">
			<?php get_template_part('content', 'slideshow'); ?>
		</div>
	</div>

<div class="welcome">
	<div class="row">
		<div class="small-12 medium-12 large-12 columns">
			<h3 class="o_txt">WELCOME</h3>
				<div class="welcome_section"><? the_content(); ?></div>
		</div>
	</div>
</div>

<div class="why_sell">
	<a id="sell"></a>
	<div class="row">
		<div class="small-12 medium-12 large-12 columns">
			<h3>WHY SELL ROYALTIES <!-- <span class="o_txt">WOODBINE ROYALTY</span> --></h3>
			
			<?php //////////////////////////////////////////////
				//$sell_list_items = cw_options_get_option( 'cwo_sell_list' );
				$sell_list_items = get_post_meta('9', '_cwmb_sell_list', true);

					
				// echo'<pre>';
				// print_r($sell_list_items);
				// echo'</pre>';

				

			?>

			
			<?if($sell_list_items[0][_cwmb_sell_title] || $sell_list_items[0][_cwmb_sell_desc]){?>
			<div class="row interests_padding">
				<div class="small-12 medium-2 large-2 columns sell_number">
					<h3 class="center">1</h3>
				</div>
				<div class="small-12 medium-10 large-10 columns sell_txt1">
					<h6 class="w_txt"><?php echo $sell_list_items[0][_cwmb_sell_title]; ?></h6>
					<p class="w_txt ">
					<?php echo  nl2br($sell_list_items[0][_cwmb_sell_desc]); ?>
				</div>
			</div>
			<?}?>
			<?if($sell_list_items[1][_cwmb_sell_title] || $sell_list_items[1][_cwmb_sell_desc]){?>
			<div class="row interests_padding">
				<div class="small-12 medium-2 large-2 columns sell_number">
					<h3 class="center">2</h3>
				</div>
				<div class="small-12 medium-10 large-10 columns sell_txt2">
					<h6 class="w_txt"><?php echo $sell_list_items[1][_cwmb_sell_title]; ?></h6>
					<p class="w_txt ">
					<?php echo nl2br($sell_list_items[1][_cwmb_sell_desc]); ?>
					</p>
				</div>
			</div>
			<?}?>
			<?if($sell_list_items[2][_cwmb_sell_title] || $sell_list_items[2][_cwmb_sell_desc]){?>
			<div class="row interests_padding">
				<div class="small-12 medium-2 large-2 columns sell_number">
					<h3 class="center">3</h3>
				</div>
				<div class="small-12 medium-10 large-10 columns sell_txt3">
					<h6 class="w_txt"><?php echo $sell_list_items[2][_cwmb_sell_title]; ?></h6>
					<p class="w_txt ">
					<?php echo nl2br($sell_list_items[2][_cwmb_sell_desc]); ?>
					</p>
				</div>
			</div>
			<?}?>
			<?if($sell_list_items[3][_cwmb_sell_title] || $sell_list_items[3][_cwmb_sell_desc]){?>
			<div class="row interests_padding">
				<div class="small-12 medium-2 large-2 columns sell_number">
					<h3 class="center">4</h3>
				</div>
				<div class="small-12 medium-10 large-10 columns sell_txt4">
					<h6 class="w_txt"><?php echo $sell_list_items[3][_cwmb_sell_title]; ?></h6>
					<p class="w_txt ">
					<?php echo nl2br($sell_list_items[3][_cwmb_sell_desc]); ?>
					</p>
				</div>
			</div>
			<?}?>
		</div>
	</div>
</div>

<div class="why_buy">
	<a id="buy"></a>
	<div class="row">
		<div class="small-12 medium-12 large-12 columns">
			<h3 class="o_txt">WHY WE BUY INTEREST</h3>
			<div class="white_txt_section">
				<?//echo wpautop(cw_options_get_option( 'cwo_buy_desc' ));?>
				<?

					$buy_content = get_post_meta('9', '_cwmb_buy_desc', true);
					echo wpautop($buy_content);

				?>
			</div>
			

		</div>
	</div>
</div>

<!-- <div class="offer"> -->
	<a id="offers"></a>
	<div class="row offer">
		<div class="small-12 medium-6 large-6 columns offer_inner">
			<div class="white_txt_section">
				<?//echo wpautop(cw_options_get_option( 'cwo_offer_desc' ));?>
				<?

					$offer1 = get_post_meta('9', '_cwmb_offer_desc', true);
					//print_r($offer1);
					echo wpautop($offer1);


				?>
			</div>

		</div>
		<div class="small-12 medium-6 large-6 columns offer_inner1">
			<div class="white_txt_section">
				<?//echo wpautop(cw_options_get_option( 'cwo_offer_desc' ));?>
				<?
					$offer2 = get_post_meta('9', '_cwmb_no_offer_desc', true);
					echo wpautop($offer2);
				?>
			</div>

		</div>
	</div><br><br>
<!-- </div> -->
<div class="findus_title">
	<a id="contact"></a>
	<div class="row">
		<h5 class="w_txt center">Please call or contact us personally with any questions you may have.</h5>
	</div>
</div>

<div class="find_us">
	<div class="row">
		<div class="small-12 medium-4 large-4 columns">
			<strong><h5>Contact US</h5></strong><br>
			<p><?echo cw_options_get_option( 'cwo_title' );?><br>
			<?echo cw_options_get_option( 'cwo_address1' );?><br>
			<?echo cw_options_get_option( 'cwo_city' );?>,
			<?echo cw_options_get_option( 'cwo_state' );?>
			<?echo cw_options_get_option( 'cwo_zip' );?><br>
			<?echo '<a href="tel:'.preg_replace("/[^0-9]/","",cw_options_get_option( 'cwo_phone' )).'">'.cw_options_get_option( 'cwo_phone' ).'</a>';?><!-- <a href="tel:18884482577">(888) 448-2577</a> --><br></p>
		</div>
		<div class="small-12 medium-8 large-8 columns">
			
			<?

				//$short_code = str_replace('\"', '"', cw_options_get_option( 'cwo_s_code' ));
				$sc = get_post_meta('9', '_cwmb_s_code', true);

				$short_code = str_replace('\"', '"',$sc);
				echo do_shortcode($short_code);
				// echo ;
			?>

			
		</div>
	</div>
</div>
<!-- </div> -->
<?php get_footer(); ?><h1 class="center"><a href="#top" id="bottom"><i class="fa fa-chevron-up o_txt"></i></a></h1>


